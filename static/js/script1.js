var themeToogle = true;
$("document").ready(function() {
    setTimeout(function() {
        $(".loading-screen").addClass("loading-screen-deactive");
    }, 1500)
    $(".change-theme-button").click(function changeTheme() {
        if (themeToogle) {
            $(this).removeClass("font-red");
            $(this).addClass("font-black");
            $(".grid-item-1").removeClass("red");
            $(".grid-item-1").addClass("black");
            $(".border-box").addClass("border-box-active");
            themeToogle = false;
        } else if (!themeToogle) {
            $(this).removeClass("font-black");
            $(this).addClass("font-red");
            $(".grid-item-1").removeClass("black");
            $(".grid-item-1").addClass("red");
            $(".border-box").removeClass("border-box-active");
            themeToogle = true;
        }
    });

    $(".accordion").accordion({
        collapsible: true,
        active : 'none',
        autoHeight: false,
        navigation: true
    });

});