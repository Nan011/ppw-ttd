function areInputValid(name, email, password) {
    let regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) &&
        name.length >= 4 && 
        password.length >= 4;
}

function showSubscriber() {
    $(".l-subscribe__subscriber").remove();
    $(".l-subscribe").append("<div class='l-subscribe__subscriber'><label>Subscriber</label><table class='t-subscribe__subscriber'></table></div>");
    $.ajax({
        url: "/lab-6/profile/subscribe/get/subscriber/",
        dataType: "json",
        success: function(data) {
            data_tags = "";
            $.each(data.data, function(index, value) {
                data_tags += "<tr>" ;
                data_tags += "<td>" + value.name + "</td>";
                data_tags += "<td><button class='c-subscribe__unsub-popup' name=" + value.email + ">Unsubscribe</button></td>";
                data_tags += "</tr>";
            });
            $(".t-subscribe__subscriber").append(data_tags);
        },
    });
}

$("document").ready(function() {
    showSubscriber();
    $(".c-subscribe__name, .c-subscribe__email, .c-subscribe__password").on("keypress", function() {
        $(".c-subscribe__submit-button");
        let name = $(".c-subscribe__password").val();
        let email = $(".c-subscribe__email").val();
        let password = $(".c-subscribe__name").val();
        if (areInputValid(name, email, password)) {
            $.ajax({
                headers: {"X-CSRFToken": $.cookie("csrftoken")},
                method: "POST",
                url: "/lab-6/profile/subscribe/validate/",
                data: {
                    email: $(".c-subscribe__email").val(),
                },
                success: function(data) {
                    let message = "";
                    if (data.email_exists) {
                        $(".c-subscribe__submit-button").addClass("c-subscribe__submit-button--disabled");
                        message = "Email tersebut sudah pernah didaftarkan sebelumnya, silakan daftar dengan email lain";
                    } else {
                        $(".c-subscribe__submit-button").removeClass("c-subscribe__submit-button--disabled");
                        message = "Belum terdaftar";
                    }
                    $(".c-subscribe__message").text(message);
                }
    
            });
        } else {
            if (!$(".c-subscribe__submit-button").hasClass("c-subscribe__submit-button--disabled")) {
                $(".c-subscribe__submit-button").addClass("c-subscribe__submit-button--disabled");
            }
            $(".c-subscribe__message").text("Input tidak valid");
        }
    });
    $(".l-subscribe").on("click", ".c-subscribe__unsub-popup", function() {
        $(".l-unsubscribe").addClass("j-unsubscribe--showup");
        $(".c-unsubscribe__popup").addClass("j-unsubscribe__popup--showup");
        $(".c-unsubscribe__wall").addClass("j-unsubscribe__wall--showup");
        let email = $(this).attr("name");

        $(".c-unsubscribe__wall").on("click", function() {
            $(".l-unsubscribe").removeClass("j-unsubscribe--showup");
            $(".c-unsubscribe__popup").removeClass("j-unsubscribe__popup--showup");
            $(".l-unsubscribe__popup").removeClass("j-unsubscribe__wall--showup");
        });

        $(".c-unsubscribe__submit-button").on("click", function() {
            $.ajax({
                headers: {"X-CSRFToken": $.cookie("csrftoken")},
                method: "POST",
                url: "/lab-6/profile/subscribe/del/",
                data: {
                    email: email,
                    password: $(".c-unsubscribe__password").val()
                },
                success: function(data) {
                    
                    let message = "";
                    if (data.user_exists) {
                        message = "Unsubsribe berhasil";
                        showSubscriber();
                    } else {
                        message = "Subscriber tidak ditemukan";
                    }
                    $(".c-unsubscribe__password").val("")
                    alert(message);
                }
            });
            $(".l-unsubscribe").removeClass("j-unsubscribe--showup");
            $(".c-unsubscribe__popup").removeClass("j-unsubscribe__popup--showup");
            $(".l-unsubscribe__popup").removeClass("j-unsubscribe__wall--showup");
        });
    });

    $(".c-subscribe__submit-button").on("click", function() {
        $.ajax({
            headers: {"X-CSRFToken": $.cookie("csrftoken")},
            method: "POST",
            url: "/lab-6/profile/subscribe/add/",
            data: {
                name: $(".c-subscribe__name").val(),
                email: $(".c-subscribe__email").val(),
                password: $(".c-subscribe__password").val(),
            },
            success: function(process) {
                let message = "";
                if (process.is_success) {
                    message = "Data berhasil disimpan";
                    $(".c-subscribe__name").val("");
                    $(".c-subscribe__email").val("");
                    $(".c-subscribe__password").val("");
                    $(".c-subscribe__submit-button").addClass("c-subscribe__submit-button--disabled");
                    showSubscriber();
                } else {
                    message = "Data tidak berhasil disimpan";
                }
                $(".c-subscribe__message").text(message);
            }

        });
    });
});