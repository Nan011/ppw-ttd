function setUserSessionFromServer(tokenId, name, image_url) {
    $.ajax({
        headers: {"X-CSRFToken": $.cookie("csrftoken")},
        async: "false",
        method: "POST",
        url: `/lab-9/user/${tokenId}/`,
        data: {
            "name": name,
            "image_url": image_url
        },
        dataType: "json",
        success: function(data) {
            if (data.user_exists) {
                // console.log("server data:" + data.fav_books)
                window.location.replace("/lab-9/books/");
            }
        },
    });
}

function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    $(".g-signin2").addClass("g-signin2--hide");
    $(".l-google__sign-out").removeClass("c-google__sign-out--hide");
    setUserSessionFromServer(googleUser.getAuthResponse().id_token, profile.getName(), profile.getImageUrl());
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    $(".g-signin2").removeClass("g-signin2--hide");
    $(".l-google__sign-out").addClass("c-google__sign-out--hide");
    $.ajax({
        url: `/lab-9/user/clear/session/`,
        dataType: "json",
        success: function(data) {
            if (data.success) {
                console.log("Success");
            }
        },
    });
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });
}

$("document").ready(function() {
    $(".l-google__sign-out").addClass("c-google__sign-out--hide");
});