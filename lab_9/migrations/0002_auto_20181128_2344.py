# Generated by Django 2.1.1 on 2018-11-28 16:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_9', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='booksuser',
            name='token_id',
        ),
        migrations.AddField(
            model_name='booksuser',
            name='user_id',
            field=models.TextField(null=True),
        ),
    ]
