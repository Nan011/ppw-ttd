from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from google.oauth2 import id_token
from google.auth.transport import requests
from lab_9 import models
import requests as requests_2
import json
CLIENT_ID = "705082240504-7d7j8p2jtai146pgl6vte9ucggddrp70.apps.googleusercontent.com"
# Create your views here.
def books_api(request):
    return select_books_api(request, "quilting")

def select_books_api(request, name):
    response = requests_2.get("https://www.googleapis.com/books/v1/volumes?q=" + name)
    books_data = response.json()
    books_data = {"books": books_data["items"]}
    books_json_data = json.dumps(books_data)
    return HttpResponse(books_json_data, content_type="application/json")

def login(request):
    data = {}
    return render(request, "login.html", data)

def user(request, token):
    userid = get_user_id(token)
    data = dict()
    if userid != None:
        user = models.BooksUser.objects.all().filter(user_id = userid).first()
        if (user == None):
            data["user_exists"] = False
            data["fav_books"] = ""
            new_user = models.BooksUser(user_id = userid, fav_books="")
            new_user.save()
            user = new_user
        else:
            data["user_exists"] = True
            data["fav_books"] = user.fav_books
        request.session["user_id"] = userid
        request.session["name"] = request.POST["name"]
        request.session["image_url"] = request.POST["image_url"]
    return JsonResponse(data)

def update_user(request, token):
    data = {}
    userid = request.session["user_id"]
    if request.method == "POST":
        try:
            user = models.BooksUser.objects.all().filter(user_id = userid).first();
            if request.POST["opeType"] == "true":
                user.fav_books = user.fav_books + request.POST["bookId"] + ";"

            else:
                user.fav_books = user.fav_books.replace(request.POST["bookId"] + ";", "")
            user.save()
            data = {
                "success": True,
                "fav_books": user.fav_books,
            
            }
        except KeyError:
            data = {
                "success": False,
            }    
    else:
        data = {
            "success": False,
        }
    return JsonResponse(data)

def check_session(request):
    data = {
        "user_exists": False
    }
    try:
        userid = request.session["user_id"] 
        data["user_books"] = models.BooksUser.objects.all().filter(user_id = userid).first().fav_books
        data["name"] = request.session["name"]
        data["image_url"] = request.session["image_url"]
        data["user_exists"] = True
        print("success")
    except KeyError:
        pass
    return JsonResponse(data)    

def clear_session(request):
    data = dict()
    try:
        del request.session["user_exists"]
        del request.session["user_id"]
        data = {
            "success": True
        }
    except KeyError:
        data = {
            "success": False
        }
    return JsonResponse(data)

def get_user_id(token):
    try:
        # Specify the CLIENT_ID of the app that accesses the backend:
        idinfo = id_token.verify_oauth2_token(token, requests.Request(), CLIENT_ID)

        # Or, if multiple clients access the backend server:
        # idinfo = id_token.verify_oauth2_token(token, requests.Request())
        # if idinfo['aud'] not in [CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]:
        #     raise ValueError('Could not verify audience.')

        if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
            raise ValueError('Wrong issuer.')

        # If auth request is from a G Suite domain:
        # if idinfo['hd'] != GSUITE_DOMAIN_NAME:
        #     raise ValueError('Wrong hosted domain.')

        # ID token is valid. Get the user's Google Account ID from the decoded token.
        userid = idinfo['sub']
        return userid
    except ValueError:
        # Invalid token
        return None

def index(request):
    data = {}
    return render(request, "display.html", data)    