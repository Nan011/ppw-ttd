from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import resolve
from django.utils import timezone
from selenium import webdriver
import unittest
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from . import views
from . import models, forms
# Create your tests here.
class SubscribeWebserviceTest(TestCase):
	def test_url_is_correct(self):
		response = Client().get("/lab-6/profile/subscribe/validate/")
		self.assertEqual(response.status_code, 200)
		response = Client().get("/lab-6/profile/subscribe/add/")
		self.assertEqual(response.status_code, 200)
		response = Client().get("/lab-6/profile/subscribe/get/subscriber/")
		self.assertEqual(response.status_code, 200)

	def test_function_works(self):
		response = resolve("/lab-6/profile/subscribe/validate/")
		self.assertEqual(response.func, views.validate_subscriber)
		response = resolve("/lab-6/profile/subscribe/add/")
		self.assertEqual(response.func, views.add_subscriber)
		response = resolve("/lab-6/profile/subscribe/get/subscriber/")
		self.assertEqual(response.func, views.get_subscriber)
		response = resolve("/lab-6/profile/subscribe/del/")
		self.assertEqual(response.func, views.del_subscriber)
		
	def test_subscriber_email(self):
		response = Client().post("/lab-6/profile/subscribe/add/", {"name": "test", "email": "test@gmail.com", "password": "password"})
		subscribers = models.Subscriber.objects.all()
		self.assertEqual(len(subscribers), 1)
		response = Client().post("/lab-6/profile/subscribe/validate/", {"email": "test@gmail.com"})
		self.assertEqual(response.status_code, 200)
		Client().post("/lab-6/profile/subscribe/del/", {"email": "test@gmail.com", "password": "password"})
		subscribers = models.Subscriber.objects.all()
		self.assertEqual(len(subscribers), 0)
		

class IndexUnitTest(TestCase):
	def test_lab_6_url_is_exist(self):
		response = Client().get("/lab-6/")
		self.assertEqual(response.status_code, 200)

	def test_funct_is_exist(self):
		stuffs = resolve("/lab-6/")
		self.assertEqual(stuffs.func, views.index)

	def test_lab_6_template_is_correct(self):
		response = Client().get("/lab-6/")
		self.assertTemplateUsed(response, "index.html")

	def create_lab6_html_text(self):
		request = HttpRequest()
		response = views.index(request)
		html_response = response.content.decode("utf8").lower()
		return html_response

	def test_greeting_word_is_exist(self):
		html_response = self.create_lab6_html_text()
		self.assertIn("hello,", html_response)
		self.assertIn("apa kabar?", html_response)

	def test_form_is_exist_in_page(self):
		html_response = self.create_lab6_html_text().replace(" ", "")
		self.assertIn("<form", html_response.lower())
		self.assertIn("</form>", html_response.lower())


	def test_form_is_valid(self):
		form = forms.StatusPostForm(data={
				"description": "An area which is rarely used in Facebook is the Notes section. It is a writing area which many bloggers find useful. The reason is because Facebook Notes does not have a character limit, as of yet. Users are directed to this area if they have very long status or comment to make. This can be used to someone's advantage if they have a longer post to make and wish to share it with their friends through tagging."
			})
		self.assertFalse(form.is_valid())

	def test_can_save_post_in_database(self):
		models.StatusPost.objects.create(description="Test")
		objs = models.StatusPost.objects.all()
		self.assertEqual(len(objs), 1)
		self.assertEqual(objs.first().description, "Test")

	def test_datetime_in_database_is_valid(self):
		obj = models.StatusPost(description="Terserah")
		now = timezone.now()
		self.assertEqual(obj.date.date(), now.date())
		self.assertEqual(obj.date.hour, now.hour)
		self.assertEqual(obj.date.minute, now.minute)
		self.assertEqual(obj.date.second, now.second)

	def test_can_save_post_from_form(self):
		response = Client().post("/lab-6/", {"description": "sedang bermimpi"})
		objs = models.StatusPost.objects.all()
		self.assertEqual(response.status_code, 302)
		self.assertEqual(len(objs), 1)
		self.assertEqual(objs.first().description, "sedang bermimpi")

	def test_post_is_already_in_landing_page(self):
		Client().post("/lab-6/", {"description": "sedang bermimpi"})
		response = self.create_lab6_html_text()
		self.assertIn("sedang bermimpi", response)

	def test_can_delete_post(self):
		Client().post("/lab-6/", {"description": "sedang bermimpi"})
		obj = models.StatusPost.objects.all().first()
		views.delete_status(HttpRequest(), obj.id)
		self.assertEqual(len(models.StatusPost.objects.all()), 0)

class ProfileUnitTest(TestCase):
	def test_profile_url_is_exist(self):
		response = Client().get("/lab-6/profile/")
		self.assertEqual(response.status_code, 200)

	def test_profile_funct_is_exist(self):
		stuffs = resolve("/lab-6/profile/")
		self.assertEqual(stuffs.func, views.profile)

	def test_profile_template_is_correct(self):
		response = Client().get("/lab-6/profile/")
		self.assertTemplateUsed(response, "profile.html")

class SubscribeTest(TestCase):
	def test_profile_url_is_exist(self):
		response = Client().get("/lab-6/profile/subscribe/")
		self.assertEqual(response.status_code, 200)

	def test_profile_funct_is_exist(self):
		stuffs = resolve("/lab-6/profile/subscribe/")
		self.assertEqual(stuffs.func, views.subscribe)

	def test_profile_template_is_correct(self):
		response = Client().get("/lab-6/profile/subscribe/")
		self.assertTemplateUsed(response, "subscribe.html")

class NewStatusTest(unittest.TestCase):
	def setUp(self):
		self.main_link = "http://localhost:8000/lab-6/"
		options = Options()
		options.add_argument("--headless")
		options.add_argument("--no-sandbox")
		options.add_argument("--disable-dev-shm-usage")
		self.browser = webdriver.Chrome("/usr/bin/chromedriver", chrome_options=options)

	def tearDown(self):
		self.browser.quit()

	def test_new_status_is_saved_and_posted(self):
		self.browser.get(self.main_link)
		status_message = "Coba Coba"
		status_input = self.browser.find_element_by_xpath("//*[@id='id_description']")
		status_input.send_keys(status_message)
		status_input.submit()
		self.browser.get(self.main_link)
		self.assertIn(status_message, self.browser.page_source)

	def test_new_status_can_be_deleted(self):
		self.browser.get(self.main_link)
		status_message = "This is not unique status"
		status_input = self.browser.find_element_by_xpath("//*[@id='id_description']")
		status_input.send_keys(status_message)
		status_input.submit()
		self.browser.get(self.main_link)
		delete_button = self.browser.find_elements_by_xpath("/html/body/div/div[3]/div/a")[-1]
		delete_button.click()
		self.assertNotIn(status_message, self.browser.page_source)

	def test_hello_world_container_is_on_the_top(self):
		self.browser.get(self.main_link)
		self.assertTrue(
		    self.browser.find_elements_by_css_selector("body > div.page-grid-con > div.grid-item-1")[0]
		    .is_displayed()
		);

	def test_input_status_after_hello_world_container(self):
		self.browser.get(self.main_link)
		self.assertTrue(
		    self.browser.find_elements_by_css_selector("body > div.page-grid-con > div.grid-item-1 + div.grid-item-3")[0]
		    .is_displayed()
		);

	def test_hello_world_background_color(self):
		self.browser.get(self.main_link)
		background = self.browser.find_elements_by_xpath("/html/body/div/div[1]")[0]
		self.assertEqual(background.value_of_css_property("background-color"), "rgba(7, 1, 48, 1)")

	def test_layout_must_be_grid(self):
		self.browser.get(self.main_link)
		background = self.browser.find_elements_by_xpath("/html/body/div")[0]
		self.assertEqual(background.value_of_css_property("display"), "grid")



class DisplayProfileTest(unittest.TestCase):
	def setUp(self):
		options = Options()
		options.add_argument("--headless")
		options.add_argument("--no-sandbox")
		options.add_argument("--disable-dev-shm-usage")
		self.browser = webdriver.Chrome("/usr/bin/chromedriver", chrome_options=options)
		self.main_link = "http://localhost:8000/lab-6/profile"
	def tearDown(self):
		self.browser.quit()

	def test_profile_stuff_is_existed(self):
		self.browser.get(self.main_link)
		self.assertIn("nandhika prayoga", self.browser.page_source.lower())
		self.assertIn("18 years old", self.browser.page_source.lower())
		self.assertIn("live in indonesia", self.browser.page_source.lower())
		self.assertIn("study at universitas indonesia", self.browser.page_source.lower())

	def test_change_theme_button_is_on_the_top(self):
		self.browser.get(self.main_link)
		button = self.browser.find_elements_by_css_selector("body > .page-grid-con > .change-theme-button")[0]
		self.assertTrue(button.is_displayed());
		self.assertIn("Ubah Tema", button.get_attribute("innerHTML"))

	def test_accordion_is_on_the_bottom(self):
		self.browser.get(self.main_link)
		self.assertTrue(
		    self.browser.find_elements_by_css_selector("body > .page-grid-con > .accordion-con")[0]
		    .is_displayed()
		);

	def test_change_theme_work(self):
		self.browser.get(self.main_link)
		background = self.browser.find_elements_by_xpath("/html/body/div/div[1]")[0]
		self.assertEqual(background.value_of_css_property("background-color"), "rgba(187, 0, 0, 1)")
		button = self.browser.find_elements_by_xpath("/html/body/div/button")[0]
		button.send_keys(Keys.ENTER)
		self.assertEqual(background.value_of_css_property("background-color"), "rgba(17, 17, 17, 1)")

	def test_at_least_there_are_three_accordion(self):
		self.browser.get(self.main_link)
		accordion_label = self.browser.find_elements_by_tag_name("label")
		self.assertTrue(len(accordion_label) >= 3)

	def test_accordion_exists_and_works(self):
		self.browser.get(self.main_link)
		accordion_label = self.browser.find_elements_by_tag_name("label")
		self.assertIn("Skills", accordion_label[0].get_attribute("innerHTML"))
		self.assertIn("Hobbies", accordion_label[1].get_attribute("innerHTML"))
		self.assertIn("Activities", accordion_label[2].get_attribute("innerHTML"))
		accordion_head = self.browser.find_elements_by_tag_name("h3")
		self.assertIn("Programming", accordion_head[0].get_attribute("innerHTML"))
		self.assertIn("Languages", accordion_head[1].get_attribute("innerHTML"))
		self.assertIn("Cooking", accordion_head[2].get_attribute("innerHTML"))
		self.assertIn("Repairing", accordion_head[3].get_attribute("innerHTML"))
		self.assertIn("Sleep", accordion_head[4].get_attribute("innerHTML"))
		self.assertIn("Study", accordion_head[5].get_attribute("innerHTML"))
		self.assertIn("Sports", accordion_head[6].get_attribute("innerHTML"))
		self.assertIn("Drawing", accordion_head[7].get_attribute("innerHTML"))
		self.assertIn("Study at University", accordion_head[8].get_attribute("innerHTML"))
		self.assertIn("Teaching", accordion_head[9].get_attribute("innerHTML"))
		self.assertIn("Research", accordion_head[10].get_attribute("innerHTML"))
		self.assertIn("Create My Own Website", accordion_head[11].get_attribute("innerHTML"))
		for head in accordion_head:
			head.send_keys(Keys.ENTER)
			head.send_keys(Keys.ENTER)



	