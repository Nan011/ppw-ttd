from django.test import TestCase, Client
from django.urls import resolve
from lab_9 import views
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class ApiTest(TestCase):
    def test_url_is_correct(self):
        response = Client().get("/lab-9/api/books/")
        self.assertEqual(response.status_code, 200)

    def test_function_works(self):
        response = resolve("/lab-9/api/books/")
        self.assertEqual(response.func, views.books_api)

    def test_selected_url_is_correct(self):
        response = Client().get("/lab-9/api/books/contoh/")
        self.assertEqual(response.status_code, 200)
    
    def test_selected_function_works(self):
        response = resolve("/lab-9/api/books/contoh/")
        self.assertEqual(response.func, views.select_books_api)

class LoginTest(TestCase):
    def test_url_is_correct(self):
        response = Client().get("/lab-9/login/")
        self.assertEqual(response.status_code, 200)
    def test_function_works(self):
        response = resolve("/lab-9/login/")
        self.assertEqual(response.func, views.login)
    def test_template_exists(self):
        response = Client().get("/lab-9/login/")
        self.assertTemplateUsed(response, "login.html")
    

class BooksTest(TestCase):
    def test_url_is_correct(self):
        response = Client().get("/lab-9/books/")
        self.assertEqual(response.status_code, 200)
    def test_function_works(self):
        response = resolve("/lab-9/books/")
        self.assertEqual(response.func, views.index)
    def test_template_exists(self):
        response = Client().get("/lab-9/books/")
        self.assertTemplateUsed(response, "display.html")

class BooksFunctionalTest(unittest.TestCase):
    def setUp(self):
        self.main_link = "http://localhost:8000/lab-9/books/"
        options = Options()
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        self.browser = webdriver.Chrome("/usr/bin/chromedriver", chrome_options=options)
        self.browser.get(self.main_link)
    
    def tearDown(self):
        self.browser.quit()
    
    def test_table_exists(self):
        self.assertTrue(self.browser.find_element_by_xpath("/html/body/table").is_displayed())

    def test_table_header_exists(self):
        header_table_tag = self.browser.find_element_by_xpath("/html/body/table/thead/tr")
        header_tags = self.browser.find_elements_by_tag_name("th")
        self.assertEqual(header_tags[0].get_attribute("innerHTML"), "Title")
        self.assertEqual(header_tags[1].get_attribute("innerHTML"), "Authors")
        self.assertEqual(header_tags[2].get_attribute("innerHTML"), "Publisher")
        self.assertEqual(header_tags[3].get_attribute("innerHTML"), "Rating")      

    def test_star_counter_is_displayed(self):
        self.assertTrue(self.browser.find_elements_by_css_selector("body div.star-counter-con > .star-icon")[0].is_displayed())
        counter_tag = self.browser.find_elements_by_css_selector("body div.star-counter-con > .star-icon + .counter-number")[0]
        self.assertTrue(counter_tag.is_displayed())
        self.assertEqual(counter_tag.get_attribute("innerHTML"), "0")


        

