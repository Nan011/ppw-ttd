from django.shortcuts import render, redirect
from django.http import JsonResponse
from . import forms, models

def index(request):
	if request.method == "POST":
		form = forms.StatusPostForm(request.POST)
		if form.is_valid():
			form_description = form.cleaned_data["description"]
			new_status_post = models.StatusPost(description=form_description)
			new_status_post.save()
		return redirect(index)
	else:
		form = forms.StatusPostForm()
		posts = models.StatusPost.objects.all()
		data = {
			"form": form,
			"posts": posts,
			"post_exists": posts.first() != None,
		}
		return render(request, "index.html", data)

def delete_status(request, pk):
	models.StatusPost.objects.get(pk=pk).delete()
	return redirect(index)

def profile(request):
	data = {
		"accordions": models.Accordion.objects.all(),
	}
	return render(request, "profile.html", data)

def subscribe(request):
	data = {"form": forms.SubscribeForm}
	return render(request, "subscribe.html", data)

def validate_subscriber(request):
	email_exists = False
	if request.method == "POST":
		if (models.Subscriber.objects.all().filter(email=request.POST["email"].lower()).first() != None):
			email_exists = True
			
	return JsonResponse({
		"email_exists": email_exists,
	})
def add_subscriber(request):
	is_success = False
	if request.method == "POST":
		models.Subscriber(
			name=request.POST["name"], 
			email=request.POST["email"].lower(), 
			password=request.POST["password"]).save()
		is_success = True
	return JsonResponse({
		"is_success": is_success,
	})

def get_subscriber(request):
	return JsonResponse({"data": list(models.Subscriber.objects.values("name", "email"))})


def del_subscriber(request):
	user_exists = False
	if request.method == "POST":
		subscribe = models.Subscriber.objects.all().filter(
				email=request.POST["email"].lower(),
				password=request.POST["password"]
			).first()
		if (subscribe != None):
			subscribe.delete()
			user_exists = True
	return JsonResponse({"user_exists": user_exists})

		