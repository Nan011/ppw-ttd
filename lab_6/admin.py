from django.contrib import admin
from lab_6 import models
# Register your models here.
admin.site.register(models.StatusPost)
admin.site.register(models.Accordion)
admin.site.register(models.Subscriber)